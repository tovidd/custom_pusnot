import 'package:custom_pusnot/helper/helper.dart';
import 'package:flutter/material.dart';

class Homepage extends StatefulWidget {
  static const String routeName = '/homepage';

  const Homepage() : super(key: const Key('Homepage'));

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          Image.network(
            'https://w0.peakpx.com/wallpaper/389/163/HD-wallpaper-waterland-nature-portrait-view-river-sky-water.jpg',
            fit: BoxFit.fill,
            width: double.infinity,
          ),
          ListView(
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            children: [1, 2, 3, 4, 5, 6, 7, 8, 9]
                .map(
                  (e) => GestureDetector(
                      onTap: () {
                        Map<dynamic, dynamic> payload = {
                          'title': 'title $e',
                          'body': 'body $e',
                          'key': 'navigation_business',
                          'icon': (e % 4 == 0)
                              ? 'ic_notification'
                              : (e % 3 == 0)
                                  ? 'ic_warning'
                                  : (e % 2 == 0)
                                      ? 'ic_health'
                                      : (e % 1 == 0)
                                          ? 'ic_gojek_pink'
                                          : null,
                          'colorized': e % 2 == 0 ? true : false,
                        };
                        NotificationHelper.showNotification(payload);
                      },
                      child: Container(
                        height: 100,
                        alignment: Alignment.center,
                        color: Colors.black38,
                        margin: const EdgeInsets.only(bottom: 100),
                        child: Text(e.toString()),
                      )),
                )
                .toList(),
          ),
        ],
      ),
    );
  }
}
