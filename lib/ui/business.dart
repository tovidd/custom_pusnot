import 'package:flutter/material.dart';

class Business extends StatefulWidget {
  static const String routeName = '/business';

  const Business() : super(key: const Key('Business'));

  @override
  State<Business> createState() => _BusinessState();
}

class _BusinessState extends State<Business> {
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Text('Business'),
      ),
    );
  }
}
