import 'package:flutter/material.dart';

class School extends StatefulWidget {
  static const String routeName = '/school';

  const School() : super(key: const Key('School'));

  @override
  State<School> createState() => _SchoolState();
}

class _SchoolState extends State<School> {
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Text('School'),
      ),
    );
  }
}
