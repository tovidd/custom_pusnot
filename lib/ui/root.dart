import 'package:custom_pusnot/ui/ui.dart';
import 'package:flutter/material.dart';

class Root extends StatefulWidget {
  static const String routeName = '/';

  const Root() : super(key: const Key('Root'));

  @override
  State<Root> createState() => _RootState();
}

class _RootState extends State<Root> {
  int _selectedIndex = 0;
  late List<Widget> _widgetOptions;

  @override
  void initState() {
    _widgetOptions = [
      const Homepage(),
      const Business(),
      const School(),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(child: _widgetOptions.elementAt(_selectedIndex)),
      extendBody: true,
      bottomNavigationBar: Theme(
        data: ThemeData(
          canvasColor: Colors.transparent,
          backgroundColor: Colors.transparent,
        ),
        child: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
            BottomNavigationBarItem(icon: Icon(Icons.business), label: 'Business'),
            BottomNavigationBarItem(icon: Icon(Icons.school), label: 'School'),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.amber[800],
          unselectedItemColor: Colors.amber[100],
          onTap: (i) => setState(() => _selectedIndex = i),
        ),
      ),
    );
  }
}
