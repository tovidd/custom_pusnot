import 'package:custom_pusnot/helper/app_action.dart';
import 'package:custom_pusnot/helper/helper.dart';
import 'package:custom_pusnot/ui/ui.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp() : super(key: const Key('MyApp'));

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final NotificationHelper _notificationHelper = NotificationHelper.instance;
  final AppAction _appAction = AppAction.instance;

  @override
  void initState() {
    _notificationHelper.initialize();
    _appAction.initialize();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Custom Pusnot',
      theme: ThemeData(primarySwatch: Colors.blue),
      navigatorKey: Utils.navigatorKey,
      routes: {
        Root.routeName: (c) => const Root(),
        Homepage.routeName: (c) => const Homepage(),
        Business.routeName: (c) => const Business(),
        School.routeName: (c) => const School(),
      },
    );
  }
}
