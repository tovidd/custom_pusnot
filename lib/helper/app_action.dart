import 'package:custom_pusnot/helper/helper.dart';
import 'package:flutter/material.dart';
import 'package:quick_actions/quick_actions.dart';

class AppAction {
  AppAction._privateConstructor();

  static final AppAction instance = AppAction._privateConstructor();

  final QuickActions _quickActions = const QuickActions();

  static const String _actionPusnot = 'action_pusnot';
  static const String _actionHelp = 'action_help';

  Future<void> initialize() async {
    _quickActions.initialize((shortcutType) {
      switch (shortcutType) {
        case _actionPusnot:
          Map<dynamic, dynamic> payload = {
            'title': 'Title pusnot',
            'body': 'Body pusnot',
            'key': 'navigation_business',
            'icon': 'ic_health',
            'colorized': true,
          };
          NotificationHelper.showNotification(payload);
          break;
        case _actionHelp:
          ScaffoldMessenger.of(Utils.navigatorKey.currentState!.context).showSnackBar(
            const SnackBar(content: Text('Action not valid')),
          );
          break;
        default:
          break;
      }
    });

    _quickActions.setShortcutItems(<ShortcutItem>[
      const ShortcutItem(type: _actionPusnot, localizedTitle: 'Pusnot', icon: '@drawable/warning'),
      const ShortcutItem(type: _actionHelp, localizedTitle: 'Help', icon: '@drawable/ic_health')
    ]);
  }
}
