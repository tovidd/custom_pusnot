import 'dart:convert';

import 'package:custom_pusnot/helper/utils.dart';
import 'package:custom_pusnot/ui/ui.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationHelper {
  /// singleton constructor
  NotificationHelper._privateConstructor();

  static final NotificationHelper instance = NotificationHelper._privateConstructor();

  static FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  /// top level function
  static Future<void> _onBackgroundMessage(RemoteMessage message) async {
    showNotification(message.data);
  }

  static AndroidNotificationChannel channel = const AndroidNotificationChannel(
    'custom_pusnot',
    'Custom Pusnot',
    description: 'Notification Channel',
    importance: Importance.max,
  );

  static AndroidNotificationChannel channel2 = const AndroidNotificationChannel(
    'custom_pusnot_2',
    'Custom Pusnot 2',
    description: 'Notification Channel 2',
    importance: Importance.max,
    sound: RawResourceAndroidNotificationSound('ringtone_notification'),
  );

  Future<void> initialize() async {
    /// create android notification channel
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel2);

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('ic_notification');

    final IOSInitializationSettings initializationSettingsIOS = IOSInitializationSettings(
      onDidReceiveLocalNotification: _onDidReceiveLocalNotification,
      defaultPresentAlert: true,
      defaultPresentBadge: true,
      defaultPresentSound: true,
    );

    final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
    );

    /// initialize plugin setting & action
    flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: _onSelectNotification,
    );

    _initializeFCM();
  }

  void _onSelectNotification(String? payload) async {
    if (payload == null) return;
    debugPrint('notification payload: $payload');
    appRoute(payload);
  }

  /// iOS < 10
  void _onDidReceiveLocalNotification(int? id, String? title, String? body, String? payload) {
    if (payload == null) return;
    debugPrint('notification payload: $payload');
    appRoute(payload);
  }

  Future<void> appRoute(String payload) async {
    Map<dynamic, dynamic> message = jsonDecode(payload);

    String? key = message['key'];
    switch (key) {
      case 'navigation_homepage':
        await Utils.navigatorKey.currentState?.pushNamed(Homepage.routeName);
        break;
      case 'navigation_business':
        await Utils.navigatorKey.currentState?.pushNamed(Business.routeName);
        break;
      default:
        ScaffoldMessenger.of(Utils.navigatorKey.currentState!.context).showSnackBar(
          const SnackBar(content: Text('Action not valid')),
        );
        break;
    }
  }

  static Future<void> showNotification(Map<dynamic, dynamic> payload) async {
    debugPrint(jsonEncode(payload));
    String? title = payload['title'];
    String? body = payload['body'];
    String? color = payload['color'];
    String? icon = payload['icon'] ?? 'ic_notification';
    bool? colorized = (payload['colorized'] ?? 'true') == 'true' ? true : false;
    String? channelId = payload['channel_id'];

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      channelId ?? channel.id,
      channel.name,
      channelDescription: channel.description,
      importance: Importance.max,
      priority: Priority.max,
      icon: icon,
      color: color != null ? Color(int.parse(color.replaceAll('#', '0xff'))) : null,
      colorized: colorized,
    );
    var iOSPlatformChannelSpecifics = const IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics,
    );

    /// show notification
    flutterLocalNotificationsPlugin.show(
      DateTime.now().millisecond,
      title,
      body,
      platformChannelSpecifics,
      payload: jsonEncode(payload),
    );
  }

  Future<void> _initializeFCM() async {
    /// initialize firebase core
    await Firebase.initializeApp();

    /// after initialized Firebase, should call getToken to release the Pusnot Cache
    await FirebaseMessaging.instance.getToken().then((v) => debugPrint(v));

    /// background pusnot manual handle
    FirebaseMessaging.onBackgroundMessage(_onBackgroundMessage);

    /// foreground pusnot manual handle
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      showNotification(event.data);
    });

    /// notification onClicked background & foreground
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      appRoute(message.data.toString());
    });
  }
}
